import Twitter from 'twitter';
import { path } from "ramda";
import ResponseText from 'mitsi_bot_lib/dist/src/ResponseText';
import ServiceInput from 'mitsi_bot_lib/dist/src/ServiceInput';

export default class TwitterWrapperApi {
  private client: Twitter;

  constructor(consumer_key: string,
    consumer_secret: string,
    access_token_key: string,
    access_token_secret: string
  ) {
    this.client = new Twitter({
      consumer_key,
      consumer_secret,
      access_token_key,
      access_token_secret,
    });
  }

  public sendMessage(result: ServiceInput): Promise<ResponseText> {
    return new Promise((resolve, _reject) => {
      const twitterMessage = path(
        ["parameters", "fields", "message", "stringValue"],
        result
      );
      if (!twitterMessage) {
        const userResponse = "J'ai pas compris :D";
        resolve(new ResponseText(userResponse));
        return;
      }
      this.client.post("statuses/update", { status: twitterMessage }, (
        error,
        tweet,
        response
      ) => {
        if (error) {
          const userResponse = `J'ai voulu tweeter : ${twitterMessage}, mais j'ai pas réussi.`;
          resolve(new ResponseText(userResponse));
          return;
        }
        const userResponse = "Fait ;)";
        resolve(new ResponseText(userResponse));
      });
    });
  }
}