import express, { Response, Request } from "express";
const bodyParser = require("body-parser");
const cors = require("cors");
const config = require('config-yml');
import Twitter from "./TwitterWrapperApi";
import ResponseText from "mitsi_bot_lib/dist/src/ResponseText";
import ServiceInput from "mitsi_bot_lib/dist/src/ServiceInput";
import BaseService from "mitsi_bot_lib/dist/src/BaseService";

class Server extends BaseService {
    onPost(request: Request, response: Response): void {
        const twitter = new Twitter(
            config.app.consumer_key,
            config.app.consumer_secret,
            config.app.access_token_key,
            config.app.access_token_secret,
        );
        twitter.sendMessage(request.body as ServiceInput)
            .then((res: ResponseText) => {
                response.json(res);
            }).catch((err: Error) => {
                console.error(err);
                response.json(new ResponseText("Weather error"));
            });
    }
}

new Server(express()).startServer();
