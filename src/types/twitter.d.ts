declare module "twitter" {
    import {  Status as Tweet } from 'twitter-d';

    interface twitterOptions {
       consumer_key: string,
       consumer_secret: string,
       access_token_key: string,
       access_token_secret: string
    }

    interface ChatResponse {
        chat_id: number,
        text: string
    }
    
    interface resultCallback { (error: Error, tweet: Tweet, response: Response): void }

    class Twitter {
        constructor(options: twitterOptions);
        public post(uri: string, parameters: any, callback: resultCallback): void;
    }

    export = Twitter;
}
